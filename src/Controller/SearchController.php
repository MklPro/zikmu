<?php

namespace App\Controller;

use App\Form\SearchType;
use App\Repository\AlbumRepository;
use App\Repository\ArtistRepository;
use App\Repository\GenreRepository;
use App\Repository\PlaylistRepository;
use App\Repository\SongRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
        #[Route('/search', name: 'search')]
//        public function search(): Response
//        {
//                $form = $this->createFormBuilder()
//                        ->add('query', TextType::class)
//                        ->add('search', SubmitType::class)
//                        ->getForm(SearchType::class);
//
//                return $this->render("search/index.html.twig", [
//                        'form' => $form->createView()
//                ]);
//        }

        public function index(
                AlbumRepository    $albumRepository,
                ArtistRepository   $artistRepository,
                GenreRepository    $genreRepository,
                PlaylistRepository $playlistRepository,
                SongRepository     $songRepository,
                Request            $request): Response
        {
                $form = $this->createForm(SearchType::class);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                        $getAlbums = $form->$this->getAlbums();
                        $getArtists = $form->$this->getArtists();
                        $getGenres = $form->$this->getGenres();
                        $getPlaylists = $form->$this->getPlaylists();
                        $getSongs = $form->$this->getSongs();
                }

                $albums = $albumRepository->findSearch($getAlbums);
                $artists = $artistRepository->findSearch( $getArtists);
                $genres = $genreRepository->findSearch( $getGenres);
                $playlists = $playlistRepository->findSearch( $getPlaylists);
                $songs = $songRepository->findSearch($getSongs);

                return $this->render("search/index.html.twig", [
                        'albums' => $albums,
                        'artists' => $artists,
                        'genres' => $genres,
                        'playlists' => $playlists,
                        'songs' => $songs,
                        'form' => $form->createView()
                ]);
        }
}
