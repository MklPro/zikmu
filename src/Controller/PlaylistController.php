<?php

namespace App\Controller;

use App\Repository\PlaylistRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PlaylistController extends AbstractController
{
        private PlaylistRepository $PlaylistRepository;

        /**
         * @param PlaylistRepository $playlistRepository
         */
        public function __construct(PlaylistRepository $playlistRepository)
        {
                $this->PlaylistRepository = $playlistRepository;
        }

        #[Route('/playlist', name: 'playlists')]
        public function index(): Response
        {
                $playlistEntities = $this->PlaylistRepository->findAll();

                return $this->render('playlist/playlistList.html.twig', [
                        'playlists' => $playlistEntities,
                ]);
        }

        #[Route('/playlist/{id}', name: 'playlist')]
        public function Playlist($id): Response
        {
                $playlistEntity = $this->PlaylistRepository->find($id);

                return $this->render('playlist/playlist.html.twig', [
                        'playlist' => $playlistEntity,
                ]);
        }
}
