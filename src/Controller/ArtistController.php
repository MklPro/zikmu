<?php

namespace App\Controller;

use App\Repository\ArtistRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArtistController extends AbstractController
{
        private ArtistRepository $artistRepository;

        /**
         * @param ArtistRepository $artistRepository
         */
        public function __construct(ArtistRepository $artistRepository)
        {
                $this->artistRepository = $artistRepository;
        }

        #[Route('/artist', name: 'artists')]
        public function index(): Response
        {
                $artistEntities = $this->artistRepository->findAll();

                return $this->render('artist/artistList.html.twig', [
                        'artists' => $artistEntities,
                ]);
        }

        #[Route('/artist/{id}', name: 'artist')]
        public function artist($id): Response
        {
                $artistEntity = $this->artistRepository->find($id);

                return $this->render('artist/artist.html.twig', [
                        'artist' => $artistEntity,
                ]);
        }
}
