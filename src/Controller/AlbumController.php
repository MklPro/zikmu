<?php

namespace App\Controller;

use App\Repository\AlbumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AlbumController extends AbstractController
{
        private AlbumRepository $AlbumRepository;

        /**
         * @param AlbumRepository $albumRepository
         */
        public function __construct(AlbumRepository $albumRepository)
        {
                $this->AlbumRepository = $albumRepository;
        }

        #[Route('/album', name: 'albums')]
        public function index(): Response
        {
                $albumEntities = $this->AlbumRepository->findAll();

                return $this->render('album/albumList.html.twig', [
                        'albums' => $albumEntities,
                ]);
        }

        #[Route('/album/{id}', name: 'album')]
        public function album($id): Response
        {
                $albumEntity = $this->AlbumRepository->find($id);

                return $this->render('album/album.html.twig', [
                        'album' => $albumEntity,
                ]);
        }

}
