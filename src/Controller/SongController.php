<?php

namespace App\Controller;

use App\Repository\SongRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SongController extends AbstractController
{
        private SongRepository $SongRepository;

        /**
         * @param SongRepository $SongRepository
         */
        public function __construct(SongRepository $SongRepository)
        {
                $this->SongRepository = $SongRepository;
        }

        #[Route('/song', name: 'songs')]
        public function index(): Response
        {
                $songEntities = $this->SongRepository->findAll();

                return $this->render('song/songList.html.twig', [
                        'songs' => $songEntities,
                ]);
        }

        #[Route('/song/{id}', name: 'song')]
        public function song($id): Response
        {
                $songEntity = $this->SongRepository->find($id);

                return $this->render('song/song.html.twig', [
                        'song' => $songEntity,
                ]);
        }
}
