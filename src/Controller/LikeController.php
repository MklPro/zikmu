<?php

namespace App\Controller;

use App\Repository\AlbumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LikeController extends AbstractController
{
    #[Route('/home', name: 'like')]
    public function index(AlbumRepository $albumRepository): Response
    {
            $albumLiked = $albumRepository->findLike();

            return $this->render('home/index.html.twig', [
                        'likes' => $albumLiked,
            ]);
    }
}
