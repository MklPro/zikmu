<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

        private UserRepository $UserRepository;

        /**
         * @param UserRepository $userRepository
         */
        public function __construct(UserRepository $userRepository)
        {
                $this->UserRepository = $userRepository;
        }

        #[Route('/user', name: 'users')]
        public function index(): Response
        {
                $userEntities = $this->UserRepository->findAll();

                return $this->render('user/userList.html.twig', [
                        'users' => $userEntities,
                ]);
        }

        #[Route('/user/{id}', name: 'user')]
        public function user($id): Response
        {
                $userEntity = $this->UserRepository->find($id);
//                $albumLiked = $this->UserRepository->findAlbumLiked();

                return $this->render('user/user.html.twig', [
                        'user' => $userEntity,
//                        'albumLiked' => $albumLiked,
                ]);
        }


}
