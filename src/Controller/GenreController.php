<?php

namespace App\Controller;

use App\Repository\GenreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GenreController extends AbstractController
{

        private GenreRepository $GenreRepository;

        /**
         * @param GenreRepository $genreRepository
         */
        public function __construct(GenreRepository $genreRepository)
        {
                $this->GenreRepository = $genreRepository;
        }

        #[Route('/genre', name: 'genres')]
        public function index(): Response
        {
                $genreEntities = $this->GenreRepository->findAll();

                return $this->render('genre/genreList.html.twig', [
                        'genres' => $genreEntities,
                ]);
        }

        #[Route('/genre/{id}', name: 'genre')]
        public function genre($id): Response
        {
                $genreEntity = $this->GenreRepository->find($id);

                return $this->render('genre/genre.html.twig', [
                        'genre' => $genreEntity,
                ]);
        }
}
