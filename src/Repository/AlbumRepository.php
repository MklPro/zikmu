<?php

namespace App\Repository;

use App\Entity\Album;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Album|null find($id, $lockMode = null, $lockVersion = null)
 * @method Album|null findOneBy(array $criteria, array $orderBy = null)
 * @method Album[]    findAll()
 * @method Album[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlbumRepository extends ServiceEntityRepository
{
        public function __construct(ManagerRegistry $registry)
        {
                parent::__construct($registry, Album::class);
        }

        public function findSearch($getAlbums): array
        {
                $query = $this
                        ->createQueryBuilder('albums');

                if (!empty($searchData->q)) {
                        $query = $query
                                ->andWhere('albums.name LIKE :q')
                                ->setParameter('q', "%{$searchData->q}%")
                        ;
                }

                return $query->getQuery()->getResult();
        }

        public function findLike()
        {
                return $this->createQueryBuilder('like')
                            ->select("album_id")
                            ->where("user_id = user.id")
                            ->getQuery()
                            ->getResult();
        }

}
