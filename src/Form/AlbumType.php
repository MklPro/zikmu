<?php

namespace App\Form;

use App\Entity\Album;
use App\Entity\Artist;
use App\Entity\Song;
use App\Repository\ArtistRepository;
use App\Repository\SongRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlbumType extends AbstractType
{
        private ArtistRepository $artistRepository;
        private SongRepository $songRepository;

        /**
         * @param ArtistRepository $artistRepository
         * @param SongRepository $songRepository
         */
        public function __construct(ArtistRepository $artistRepository, SongRepository $songRepository)
        {
                $this->artistRepository = $artistRepository;
                $this->songRepository = $songRepository;
        }


        public function buildForm(FormBuilderInterface $builder, array $options): void
        {
                $builder
                        ->add('name')
                        ->add('publishedAt')
                        ->add('pathImg')
                        ->add('artists', EntityType::class, [
                                "class" => Artist::class,
                                "choice_label" => "name",
                                "choices" => $this->artistRepository->findAll(),
                                'multiple' => true
                        ])
                        ->add('songs', EntityType::class, [
                                "class" => Song::class,
                                "choice_label" => "name",
                                "choices" => $this->songRepository->findAll(),
                                'multiple' => true
                        ]);
        }

        public function configureOptions(OptionsResolver $resolver): void
        {
                $resolver->setDefaults([
                        'data_class' => Album::class,
                ]);
        }
}
