<?php

namespace App\Form;

use App\Entity\Artist;
use App\Entity\Genre;
use App\Entity\Song;
use App\Repository\ArtistRepository;
use App\Repository\GenreRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SongType extends AbstractType
{
        private ArtistRepository $artistRepository;
        private GenreRepository $genreRepository;

        /**
         * @param ArtistRepository $artistRepository
         * @param GenreRepository $genreRepository
         */
        public function __construct(ArtistRepository $artistRepository, GenreRepository $genreRepository)
        {
                $this->artistRepository = $artistRepository;
                $this->genreRepository = $genreRepository;
        }

        public function buildForm(FormBuilderInterface $builder, array $options): void
        {
                $builder
                        ->add('name')
                        ->add('duration')
                        ->add('artists', EntityType::class, [
                                "class" => Artist::class,
                                "choice_label" => "name",
                                "choices" => $this->artistRepository->findAll(),
                                'multiple' => true
                        ])
                        ->add('genres', EntityType::class, [
                                "class" => Genre::class,
                                "choice_label" => "name",
                                "choices" => $this->genreRepository->findAll(),
                                'multiple' => true
                        ]);
        }

        public function configureOptions(OptionsResolver $resolver): void
        {
                $resolver->setDefaults([
                        'data_class' => Song::class,
                ]);
        }
}
