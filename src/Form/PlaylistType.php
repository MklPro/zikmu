<?php

namespace App\Form;

use App\Entity\Playlist;
use App\Entity\Song;
use App\Repository\SongRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlaylistType extends AbstractType
{
        private SongRepository $songRepository;

        /**
         * @param SongRepository $songRepository
         */
        public function __construct(SongRepository $songRepository)
        {
                $this->songRepository = $songRepository;
        }


        public function buildForm(FormBuilderInterface $builder, array $options): void
        {
                $builder
                        ->add('name')
                        ->add('songs', EntityType::class, [
                                "class" => Song::class,
                                "choice_label" => "name",
                                "choices" => $this->songRepository->findAll(),
                                'multiple' => true
                        ]);
        }

        public function configureOptions(OptionsResolver $resolver): void
        {
                $resolver->setDefaults([
                        'data_class' => Playlist::class,
                ]);
        }
}
