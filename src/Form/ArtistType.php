<?php

namespace App\Form;

use App\Entity\Artist;
use App\Entity\Genre;
use App\Repository\GenreRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArtistType extends AbstractType
{
        private GenreRepository $genreRepository;

        /**
         * @param GenreRepository $genreRepository
         */
        public function __construct(GenreRepository $genreRepository)
        {
                $this->genreRepository = $genreRepository;
        }

        public function buildForm(FormBuilderInterface $builder, array $options): void
        {
                $builder
                        ->add('name')
                        ->add('pathImg')
                        ->add('genres', EntityType::class, [
                                "class" => Genre::class,
                                "choice_label" => "name",
                                "choices" => $this->genreRepository->findAll(),
                                'multiple' => true
                        ]);
        }

        public function configureOptions(OptionsResolver $resolver): void
        {
                $resolver->setDefaults([
                        'data_class' => Artist::class,
                ]);
        }
}
